module github.com/HatsuneMiku3939/go-trail

go 1.21.3

require (
	github.com/JohannesKaufmann/html-to-markdown v1.5.0
	github.com/sap-nocops/duckduckgogo v0.0.0-20201102135645-176990152850
	github.com/sashabaranov/go-openai v1.20.3
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
