all: help

TOOLS_DIR := $(shell pwd)/tools/.bin

.PHONY: help
help: Makefile
	@sed -n 's/^##//p' $< | awk 'BEGIN {FS = ":"}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

## generate: Generate all mocks ..etc
.PHONY: generate
generate: tools/install
	mkdir -p .tmp
	go generate ./...

## test: Run test
.PHONY: test
test: generate
	go test -timeout=30s -shuffle on -race -coverprofile=.tmp/coverage.txt -covermode=atomic ./...

## test/coverage: View coverage
.PHONY: test/coverage
test/coverage: test
	go tool cover -func=.tmp/coverage.txt
	#go tool cover -html=.tmp/coverage.txt

## lint: Run golangci-lint
.PHONY: lint generate
lint: tools/install
	$(TOOLS_DIR)/golangci-lint run -c .github/linters/.golangci.yml --out-format colored-line-number

.PHONY: lint generate
lint-ci: tools/install
	$(TOOLS_DIR)/golangci-lint run -c .github/linters/.golangci.yml

.PHONY: tools/install
## tools/install: Install all tools
tools/install:
	$(shell pwd)/tools/install all

## tools/reinstall: Reinstall all tools
tools/reinstall:
	$(shell pwd)/tools/install clear
	$(shell pwd)/tools/install all

## tools/godoc: View godoc
PKG_NAME:=$(shell cat go.mod | grep module | cut -d' ' -f2)
.PHONY: godoc
tools/godoc: tools/install
	@echo "Open http://localhost:6060/pkg/$(PKG_NAME)/?m=all on browser."
	$(TOOLS_DIR)/godoc -http localhost:6060 -play -index

