package graph

import (
	"context"
	"fmt"
	"sync"
)

// Graph is a type that represents a graph
type Graph struct {
	lock sync.RWMutex

	// name of the graph
	name string

	// state of the graph
	state *State

	// nodes of the graph
	nodes map[string]*graphNode

	// params is a map that represents the params of a node
	params *State

	// current node
	currentNode string
}

// Status is a type that represents the status of the graph
type Status string

// Terminated returns if the graph is terminated
func (s Status) Terminated() bool {
	return s == GraphStatusCompleted || s == GraphStatusFailed
}

const (
	// GraphStatusPending is the status of the graph when it is pending
	GraphStatusPending Status = "PENDING"
	// GraphStatusProcessing is the status of the graph when it is processing
	GraphStatusProcessing Status = "PROCESSING"
	// GraphStatusWaitInput is the status of the graph when it is waiting for input
	GraphStatusWaitInput Status = "WAIT_INPUT"
	// GraphStatusCompleted is the status of the graph when it is completed
	GraphStatusCompleted Status = "COMPLETED"
	// GraphStatusFailed is the status of the graph when it is failed
	GraphStatusFailed Status = "FAILED"
)

const (
	// Start is the name of the start node
	START = "START"
	// END is the name of the end node
	END = "END"
	// FAILED is the name of the failed node
	FAILED = "FAILED"
)

// New creates a new graph
func New(name string, opts ...Option) (*Graph, error) {
	g := &Graph{
		name:   name,
		lock:   sync.RWMutex{},
		state:  NewState(),
		nodes:  make(map[string]*graphNode),
		params: NewState(),
	}

	// Add the special nodes. START, END, FAILED
	if err := g.AddNode(START, &BaseNode{}); err != nil {
		return nil, err
	}
	if err := g.AddNode(END, &BaseNode{}); err != nil {
		return nil, err
	}
	if err := g.AddNode(FAILED, &BaseNode{}); err != nil {
		return nil, err
	}

	// Add the transitions for the special nodes
	// start Transition to end node when the graph initialized
	if err := g.AddTransition(START, END); err != nil {
		return nil, err
	}
	// failed transition to itself
	if err := g.AddTransition(FAILED, FAILED); err != nil {
		return nil, err
	}
	// end transition to itself
	if err := g.AddTransition(END, END); err != nil {
		return nil, err
	}

	// Set the start node as the next node
	g.setNextNode(g.getNode(START))

	// Apply the options
	for _, opt := range opts {
		if err := opt(g); err != nil {
			return nil, err
		}
	}

	return g, nil
}

// AddNode adds a node to the graph
func (g *Graph) AddNode(name string, node Node) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	if g.isNodeExist(name) {
		return fmt.Errorf("Node %s already exists", name)
	}

	graphNode := newGraphNode(name, node)
	return g.addNode(name, graphNode)
}

// AddTransition adds a transition to the graph
func (g *Graph) AddTransition(from string, to string) error {
	return g.AddConditionalTransition(from, func(_ context.Context, _ *State) (string, error) {
		return to, nil
	})
}

// Transition is a type that represents a transition function
type Transition func(ctx context.Context, stateSnapshot *State) (string, error)

// AddConditionalTransition adds a conditional transition to the graph
func (g *Graph) AddConditionalTransition(from string, transition Transition) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	if !g.isNodeExist(from) {
		return fmt.Errorf("Node %s does not exist", from)
	}

	node := g.getNode(from)
	node.updateTransition(transition)
	return nil
}

// Parameterize is a type that represents a parameterize function
type Parameterize func(ctx context.Context, stateSnapshot *State) ([]string, error)

// AddConditionalParameterize adds a conditional parameterize to the graph
func (g *Graph) AddConditionalParameterize(node string, parameterize Parameterize) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	if !g.isNodeExist(node) {
		return fmt.Errorf("Node %s does not exist", node)
	}

	graphNode := g.getNode(node)
	graphNode.updateParameterize(parameterize)
	return nil
}

// Invoke is a type that represents an invoke function
type Invoke func(ctx context.Context, stateSnapshot *State) (*State, error)

// AddInvoke adds an invoke function to the graph
func (g *Graph) AddInvoke(node string, invoke Invoke) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	if !g.isNodeExist(node) {
		return fmt.Errorf("Node %s does not exist", node)
	}

	graphNode := g.getNode(node)
	graphNode.updateInvoke(invoke)
	return nil
}

// PutParameter puts the parameter to the graph
func (g *Graph) PutParameter(input *State) {
	g.lock.Lock()
	defer g.lock.Unlock()

	g.updateParams(input)
}

// Result is a type that represents the result of the process
type Result struct {
	Status      Status
	CurrentNode string
}

// Process processes the graph
func (g *Graph) Process(ctx context.Context) (Result, *State, error) {
	g.lock.Lock()
	defer g.lock.Unlock()

	// Get Status, State and Current Node
	currentNode := g.getCurrentNode()
	stateSnapshot := g.state.Clone()
	status, err := g.status(ctx)
	if err != nil {
		// if there is an error, set the next node to FAILED
		g.setNextNode(g.getNode(FAILED))
		return newResult(status, currentNode), stateSnapshot, err
	}

	switch status {
	case GraphStatusCompleted, GraphStatusFailed:
		// if the graph is already completed or failed, return the status and the state
		return newResult(status, currentNode), stateSnapshot, nil
	case GraphStatusWaitInput:
		// if the graph is waiting for input, return the status and the state
		return newResult(status, currentNode), stateSnapshot, nil
	case GraphStatusPending, GraphStatusProcessing:
		// all other cases are needed to be processed
	}

	// Process the graph
	updateState, err := g.process(ctx)
	if err != nil {
		// if there is an error, set the next node to FAILED
		g.setNextNode(g.getNode(FAILED))
		return newResult(status, currentNode), stateSnapshot, err
	}

	// merge the update state with the state snapshot
	newState := stateSnapshot.Merge(updateState)

	// Transition to the next node
	nextNode, err := g.applyTransition(ctx, currentNode, newState.Clone())

	// if transitions is successful, update the state and clear the input
	if err == nil {
		g.updateState(newState)
		g.clearParams()
	}

	// set the next node
	g.setNextNode(nextNode)

	// return the status and the state
	return newResult(status, currentNode), g.state.Clone(), err
}

// applyTransition applies the transition of the graph for given node
func (g *Graph) applyTransition(ctx context.Context, from *graphNode, state *State) (nextNode *graphNode, err error) {
	// panic recovery
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic(%s) on transition(%s): %w", r, from.name, ErrNodeTransition)
			nextNode = g.getNode(FAILED)
		}
	}()

	next, err := from.Transition(ctx, state)
	if err != nil {
		return g.getNode(FAILED), err
	}

	// if the next node does not exist, return an error
	if exists := g.isNodeExist(next); !exists {
		err = fmt.Errorf("NextNode(%s) is not exist on transition(%s): %w", next, from.name, ErrNodeTransition)
		return g.getNode(FAILED), err
	}

	nextNode = g.getNode(next)
	return nextNode, nil
}

func (g *Graph) process(ctx context.Context) (updateState *State, err error) {
	// Get the current node
	currentNode := g.getCurrentNode()
	stateSnapshot := g.state.Clone()

	// panic recovery
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic(%s) on invoke(%s): %w", r, currentNode.name, ErrNodeInvoke)
			updateState = NewState()
		}
	}()

	// merge the input with the state snapshot
	stateSnapshot = stateSnapshot.Merge(g.params)

	// Invoke the current nodes
	updateState, err = currentNode.Invoke(ctx, stateSnapshot)
	if err != nil {
		// if there is an error, empty state update state
		return NewState(), err
	}

	// return the update state with input merged
	// input should be merged with the update state
	updateState = updateState.Merge(g.params)
	return updateState, nil
}

// SetEntryPoint sets the start node of the graph
func (g *Graph) SetEntryPoint(node string) error {
	// Add the transitions from start node to the given node
	return g.AddTransition(START, node)
}

// FrozenGraph is a type that represents a state of a graph.
// it can be serialized and deserialized to and from a byte slice using encoding packages
type FrozenGraph struct {
	// Name is the name of the graph
	Name string
	// State is the state of the graph
	State map[string]any
	// Params is the parameters that provied to the graph
	Params map[string]any
	// LastNode is the name of the last node that processed
	LastNode string
}

// Freeze returns a new graph that is a copy of the state of the original graph
// note that the nodes are not copied, only the state and the parameters
func (g *Graph) Freeze() *FrozenGraph {
	g.lock.RLock()
	defer g.lock.RUnlock()

	return &FrozenGraph{
		Name:     g.name,
		State:    g.state.Clone().Dump(),
		Params:   g.params.Clone().Dump(),
		LastNode: g.currentNode,
	}
}

// status returns the status of the graph
func (g *Graph) status(ctx context.Context) (Status, error) {
	// Get the current node
	current := g.getCurrentNode()
	stateSnapshot := g.state.Clone()

	// if the current node is special, return the its status
	switch current.name {
	case "START":
		return GraphStatusPending, nil
	case "END":
		return GraphStatusCompleted, nil
	case "FAILED":
		return GraphStatusFailed, nil
	}

	// get the list of parameters needed for the current node
	needed, err := current.Parameterize(ctx, stateSnapshot)
	if err != nil {
		return GraphStatusFailed, err
	}

	// if the current node need parameters, and the parameters are not provided, return WAIT_INPUT
	if !g.hasParams(needed) {
		return GraphStatusWaitInput, nil
	}

	// if the current node is not special, and it does not have input, return PROCESSING
	return GraphStatusProcessing, nil
}
