package graph

import (
	"context"
)

// setNextNode updates the current node of the graph
func (g *Graph) setNextNode(node *graphNode) {
	g.currentNode = node.name
}

// hasParams returns if the parameter is provided
func (g *Graph) hasParams(need []string) bool {
	// tese if the required parameters are provided
	for _, param := range need {
		if !g.params.Has(param) {
			return false
		}
	}

	return true
}

// updateState updates the state of the graph
func (g *Graph) updateState(newState *State) {
	g.state = newState
}

// clearParams clears the input of the graph
func (g *Graph) clearParams() {
	g.params.Clear()
}

// getNode returns the node of the graph for given name
func (g *Graph) getNode(name string) *graphNode {
	return g.nodes[name]
}

// isNodeExist returns if the given node name exists in the graph
func (g *Graph) isNodeExist(name string) bool {
	_, exists := g.nodes[name]
	return exists
}

// addNode adds a node to the graph
func (g *Graph) addNode(name string, node *graphNode) error {
	g.nodes[name] = node
	return nil
}

// updateParams updates the input of the graph
func (g *Graph) updateParams(input *State) {
	g.params = input.Clone()
}

// getCurrentNode returns the current node of the graph
func (g *Graph) getCurrentNode() *graphNode {
	return g.getNode(g.currentNode)
}

// newResult creates an update state of the graph
func newResult(status Status, currentNode Node) Result {
	name := "unknown"
	if gnode, ok := currentNode.(*graphNode); ok {
		name = gnode.name
	}

	return Result{
		Status:      status,
		CurrentNode: name,
	}
}

// graphNode is a internal type that used to represent the node in the graph
type graphNode struct {
	// node is the actual node
	node Node
	// name is the name of the node in the graph
	name string

	// invoke is the override function for the node invoke
	invoke Invoke
	// transition is the override function for the node transition
	transition Transition
	// parameterize is the override function for the node parameterize
	parameterize Parameterize
}

// Invoke implements the Node interface
func (n *graphNode) Invoke(ctx context.Context, stateSnapshot *State) (*State, error) {
	if n.invoke != nil {
		return n.invoke(ctx, stateSnapshot)
	}

	return n.node.Invoke(ctx, stateSnapshot)
}

// Transition implements the Node interface
func (n *graphNode) Transition(ctx context.Context, stateSnapshot *State) (string, error) {
	if n.transition != nil {
		return n.transition(ctx, stateSnapshot)
	}

	return n.node.Transition(ctx, stateSnapshot)
}

// Parameterize implements the Node interface
func (n *graphNode) Parameterize(ctx context.Context, stateSnapshot *State) ([]string, error) {
	if n.parameterize != nil {
		return n.parameterize(ctx, stateSnapshot)
	}

	return n.node.Parameterize(ctx, stateSnapshot)
}

// updateTransition updates the transition of the node in the graph
func (n *graphNode) updateTransition(transition Transition) {
	n.transition = transition
}

// updateParameterize updates the parameterize of the node in the graph
func (n *graphNode) updateParameterize(parameterize Parameterize) {
	n.parameterize = parameterize
}

// updateInvoke updates the invoke of the node in the graph
func (n *graphNode) updateInvoke(invoke Invoke) {
	n.invoke = invoke
}

// newGraphNode creates a new graph node
func newGraphNode(name string, node Node) *graphNode {
	return &graphNode{
		node: node,
		name: name,
	}
}
