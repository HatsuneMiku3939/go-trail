package graph

import "errors"

var (
	// ErrNodeInvoke is returned when a node fails to invoke
	ErrNodeInvoke = errors.New("Node invoke error")
	// ErrNodeTransition is returned when a transition fails
	ErrNodeTransition = errors.New("Transition error")
)
