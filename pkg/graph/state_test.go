package graph

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetAndGet(t *testing.T) {
	// test Set and Get functions set and get state correctly
	state := NewState()
	state.Set("key1", "value1")
	value := state.Get("key1")
	assert.Equal(t, "value1", value)
}

func TestClone(t *testing.T) {
	// test Clone function creates a deep copy of the state
	state := NewState()
	state.Set("key1", "value1")
	clone := state.Clone()
	state.Set("key1", "value2")

	assert.NotEqual(t, state.Get("key1"), clone.Get("key1"))
}

func TestMerge(t *testing.T) {
	// test Merge function merges two states correctly
	state1 := NewState()
	state1.Set("key1", "value1")
	state2 := NewState()
	state2.Set("key2", "value2")

	mergedState := state1.Merge(state2)
	assert.Equal(t, "value1", mergedState.Get("key1"))
	assert.Equal(t, "value2", mergedState.Get("key2"))
}

func TestEmpty(t *testing.T) {
	// test Empty function correctly determines if the state is empty
	notEmptyState := NewState()
	notEmptyState.Set("key", "value")
	emptyState := NewState()

	assert.False(t, notEmptyState.Empty())
	assert.True(t, emptyState.Empty())
}
