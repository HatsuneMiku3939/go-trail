package graph

type Option func(*Graph) error

// WithInitialState sets the initial state of the graph.
func WithInitialState(state *State) Option {
	return func(g *Graph) error {
		g.updateState(state.Clone())
		return nil
	}
}

// WithNodes sets the nodes of the graph.
func WithNodes(nodes map[string]Node) Option {
	return func(g *Graph) error {
		for name, node := range nodes {
			if err := g.AddNode(name, node); err != nil {
				return err
			}
		}

		return nil
	}
}

// WithFrozenGraph restores the graph from a frozen state.
func WithFrozenGraph(frozen *FrozenGraph) Option {
	return func(g *Graph) error {
		// Restore the state
		state := NewState(frozen.State)
		g.updateState(state)

		// Set the next node to process to the last node that was processed
		// TODO, test the last node is in the nodes map
		lastNode := g.getNode(frozen.LastNode)
		g.setNextNode(lastNode)

		// Restore the parameters provided to the graph
		params := NewState(frozen.Params)
		g.updateParams(params)

		return nil
	}
}
