package graph

import (
	"sync"
)

// State is a type that represents the state of the graph
type State struct {
	lock sync.RWMutex

	data map[string]any
}

// NewState creates a new graph state
// if you want to initialize the state with some values, you can pass them as arguments
func NewState(initialStates ...map[string]any) *State {
	state := &State{
		data: make(map[string]any),
		lock: sync.RWMutex{},
	}

	for _, initialState := range initialStates {
		for k, v := range initialState {
			state.Set(k, v)
		}
	}

	return state
}

// Clone returns a hard copy of the state
func (s *State) Clone() *State {
	s.lock.RLock()
	defer s.lock.RUnlock()

	// TODO, implement a deep copy
	hardCopy := NewState()
	for k, v := range s.data {
		hardCopy.Set(k, v)
	}

	return hardCopy
}

// Get returns the value of the state by the key.
// If the key does not exist, it returns interface{}(nil)
func (s *State) Get(key string) any {
	s.lock.RLock()
	defer s.lock.RUnlock()

	value, exists := s.data[key]
	if !exists {
		return interface{}(nil)
	}

	return value
}

// Has returns if the key exists in the state
func (s *State) Has(key string) bool {
	s.lock.RLock()
	defer s.lock.RUnlock()

	_, exists := s.data[key]
	return exists
}

// Set sets the value of the state by the key
func (s *State) Set(key string, value any) *State {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.data[key] = value
	return s
}

// Merge merges the state with the given state, and returns the new state
func (s *State) Merge(state *State) *State {
	// clone the state before lock
	clone := s.Clone()

	s.lock.Lock()
	defer s.lock.Unlock()

	for k, v := range state.data {
		clone.Set(k, v)
	}

	return clone
}

// Empty returns if the state is empty
func (s *State) Empty() bool {
	s.lock.RLock()
	defer s.lock.RUnlock()

	return len(s.data) == 0
}

// Clear clears the state
func (s *State) Clear() *State {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.data = make(map[string]any)
	return s
}

// Dump returns the state as a map
func (s *State) Dump() map[string]any {
	clone := s.Clone()

	return clone.data
}
