package graph

import (
	"context"
)

// Node is an interface that represents a common interface for the graph node
type Node interface {
	// Invoke is a method that used to invoke the node
	Invoke(ctx context.Context, stateSnapshot *State) (*State, error)
	// Transition is a method that used to determine the next node
	Transition(ctx context.Context, stateSnapshot *State) (string, error)
	// Parameterize is a method that used to tell to the graph that the node needs parameter
	Parameterize(ctx context.Context, stateSnapshot *State) ([]string, error)
}

// BaseNode is a base implementation of the node interface that does nothing
type BaseNode struct{}

// Invoke invokes the node
func (n *BaseNode) Invoke(_ context.Context, _ *State) (*State, error) {
	return NewState(), nil
}

// Transition is a method that used to determine the next node
func (n *BaseNode) Transition(_ context.Context, _ *State) (string, error) {
	return "", nil
}

// Parameterize is a method that used to tell to the graph that the node needs parameter
func (n *BaseNode) Parameterize(_ context.Context, _ *State) ([]string, error) {
	return []string{}, nil
}
