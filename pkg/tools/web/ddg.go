package web

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/tools"

	"github.com/sap-nocops/duckduckgogo/client"
	"github.com/sashabaranov/go-openai/jsonschema"
)

var _ tools.Tool = (*DuckDuckGo)(nil)

const DefaultDDGMaxResults = 10

type DuckDuckGo struct {
	maxResults int
	client     *client.DuckDuckGoSearchClient
}

func NewDDG(maxResults int) (*DuckDuckGo, error) {
	return &DuckDuckGo{
		maxResults: maxResults,
		client:     client.NewDuckDuckGoSearchClient(),
	}, nil
}

func (d *DuckDuckGo) Name() string {
	return "web-duckduck-search"
}

func (d *DuckDuckGo) Description() string {
	return `
	"A wrapper around DuckDuckGo Search."
	"Free search alternative to google and serpapi."
	"Input should be a search query."`
}

func (d *DuckDuckGo) Parameters() jsonschema.Definition {
	return jsonschema.Definition{
		Type: jsonschema.Object,
		Properties: map[string]jsonschema.Definition{
			"query": {
				Type:        jsonschema.String,
				Description: "The query to search for",
			},
			"maxResults": {
				Type:        jsonschema.Integer,
				Description: "The maximum number of results to return(max 10, default 10)",
			},
		},
		Required: []string{"query"},
	}
}

type duckDuckGoParameter struct {
	Query      string `json:"query"`
	MaxResults int    `json:"maxResults"`
}

func (d *DuckDuckGo) Call(_ context.Context, input string) (string, error) {
	// Parse input as parameters
	params := duckDuckGoParameter{}
	if err := json.Unmarshal([]byte(input), &params); err != nil {
		return "", err
	}

	if params.Query == "" {
		return "", fmt.Errorf("query is required")
	}

	if d.maxResults > DefaultDDGMaxResults {
		d.maxResults = DefaultDDGMaxResults
	}

	// do the search
	docs, err := d.client.SearchLimited(params.Query, params.MaxResults)
	if err != nil {
		return "", err
	}

	// return the results as json string
	result, err := json.MarshalIndent(docs, "", "  ")
	if err != nil {
		return "", err
	}

	return string(result), nil
}
