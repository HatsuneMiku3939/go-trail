package web

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/JohannesKaufmann/html-to-markdown/plugin"
	"github.com/sashabaranov/go-openai/jsonschema"
)

type Scrap struct {
}

// New creates a new Scrap tools
func NewScrap() (*Scrap, error) {
	return &Scrap{}, nil
}

// Name returns the name of the tool
func (s *Scrap) Name() string {
	return "web-scrap"
}

// Description returns the description of the tool
func (s *Scrap) Description() string {
	return "Scraping text contents from single web page as markdown format"
}

// Parameters returns the parameters of the tool
func (s *Scrap) Parameters() jsonschema.Definition {
	return jsonschema.Definition{
		Type: jsonschema.Object,
		Properties: map[string]jsonschema.Definition{
			"url": {
				Type:        jsonschema.String,
				Description: "The URL to scrape",
			},
		},
		Required: []string{"url"},
	}
}

type scrapParameter struct {
	URL string `json:"url"`
}

// Run runs the tool
func (s *Scrap) Call(ctx context.Context, input string) (string, error) {
	// Parse input as parameters
	params := scrapParameter{}
	if err := json.Unmarshal([]byte(input), &params); err != nil {
		return "", err
	}

	if params.URL == "" {
		return "", fmt.Errorf("url is required")
	}

	// Scrape the web page
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, params.URL, nil)
	if err != nil {
		return "", fmt.Errorf("failed to create a request: %w", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to get the web page: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("failed to get the web page: %s", resp.Status)
	}

	// Convert the web page to markdown
	html, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read the web page: %w", err)
	}

	converter := md.NewConverter("", true, nil)
	converter.Use(plugin.GitHubFlavored())
	markdown, err := converter.ConvertString(string(html))
	if err != nil {
		return "", fmt.Errorf("failed to convert the web page to markdown: %w", err)
	}

	return markdown, nil
}
