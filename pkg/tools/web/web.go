package web

import (
	"github.com/HatsuneMiku3939/go-trail/pkg/tools"
)

// All returns all the tools in this package
func All() ([]tools.Tool, error) {
	ddg, err := NewDDG(DefaultDDGMaxResults)
	if err != nil {
		return nil, err
	}

	scrap, err := NewScrap()
	if err != nil {
		return nil, err
	}

	return []tools.Tool{
		ddg,
		scrap,
	}, nil
}
