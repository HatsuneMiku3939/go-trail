package tools

import (
	"context"
	"fmt"

	"github.com/sashabaranov/go-openai/jsonschema"
)

// Tool is the interface that must be implemented by all tools
type Tool interface {
	// Name returns the name of the tool
	Name() string
	// Description returns a description of the tool
	Description() string
	// Parameters returns the JSON schema for the parameters of the tool
	Parameters() jsonschema.Definition

	// Call executes the tool with the given input and returns the result
	Call(ctx context.Context, input string) (string, error)
}

// Merge merges the given tools into a single slice
func Merge(tools []Tool, tool ...Tool) []Tool {
	return append(tools, tool...)
}

// FindTool returns a tool by name
func FindTool(tools []Tool, name string) (Tool, error) {
	for _, tool := range tools {
		if tool.Name() == name {
			return tool, nil
		}
	}

	return nil, fmt.Errorf("tool not found")
}
