package prompt

// Prompt is an interface for a prompt
type Prompt interface {
	// Prompt returns the prompt
	Format(vars ...map[string]string) (string, error)
}
