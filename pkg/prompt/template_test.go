package prompt

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlaceholder(t *testing.T) {
	assert.Equal(t, 1, 1)
}
