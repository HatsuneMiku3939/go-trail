package prompt

import (
	"bytes"
	"fmt"

	"text/template"
)

var _ Prompt = &TemplatePrompt{}

// TemplatePrompt is a struct that holds the template for the prompt
type TemplatePrompt struct {
	// The template for the prompt
	Template string
}

// NewTemplatePrompt creates a new prompt with the given template
func NewTemplatePrompt(template string) *TemplatePrompt {
	return &TemplatePrompt{Template: template}
}

// Prompt returns the prompt
func (t *TemplatePrompt) Format(vars ...map[string]string) (string, error) {
	// Parse the template
	tmpl, err := template.New("prompt").Parse(t.Template)
	if err != nil {
		return "", fmt.Errorf("Error parsing template: %s", err)
	}

	// Merge all the maps into one
	merged := make(map[string]string)
	for _, v := range vars {
		for k, val := range v {
			merged[k] = val
		}
	}

	// Execute the template
	var prompt bytes.Buffer
	err = tmpl.Execute(&prompt, vars)
	if err != nil {
		return "", fmt.Errorf("Error executing template: %s", err)
	}

	return prompt.String(), nil
}
