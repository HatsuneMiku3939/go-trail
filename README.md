# GoTrail
Streamlined Graph State Management for LLM Applications in Go

## Introduction

GoTrail is a streamlined graph state management library designed for building
Large Language Model (LLM) applications with Go. It simplifies the handling of
complex flows and state management, enabling developers to write clean and
intuitive code.

GoTrail is built with Golang, offering high scalability and maintainability.

## Core Concepts

- **Node**: A single processing unit within the graph. It can execute custom
  logic and update the state. Each node in GoTrail implements the following
  interface:
```go
type Node interface {
    // Invoke is called to process the node's logic.
    Invoke(ctx context.Context, stateSnapshot *State) (*State, error)

    // Transition determines the next node to transition to based on the current state.
    // It returns the name of the next node.
    Transition(ctx context.Context, stateSnapshot *State) (string, error)

    // Parameterize optionally specifies the parameters needed by the node.
    // It returns a list of required parameter names.
    Parameterize(ctx context.Context, stateSnapshot *State) ([]string, error)
}
```
This interface allows each node to define its own processing logic, transition
rules, and required parameters, making the graph highly customizable and
adaptable to various use cases.
- **State**: A collection of information passed between nodes, representing the
  current status of the graph. The State object allows nodes to share data and
  outcomes, ensuring seamless transitions and interactions within the graph.
- **Graph**: Comprises a set of nodes and their connections, controlling the
  entire process flow. The Graph orchestrates how nodes are executed, manages
  the overall state, and determines the path through the graph based on node
  transitions.

## Quick Start

- Include the GoTrail library in your project.
- Implement the necessary nodes and define the logic each node should perform.
- Connect the nodes to construct the graph and set its initial state.
- Initiate the graph processing and observe the state until completion.

```go
package main

import (
	"context"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

func main() {
	// Create graph and set initial state
	g, err := graph.New(
		"hello-graph",
		graph.WithInitialState(graph.NewState(map[string]any{"message": "Hello"})),
	)
	if err != nil {
		panic(err)
	}

	// Add the node
	g.AddNode("HELLO", &graph.BaseNode{})

	// Set transitions
	g.AddTransition("HELLO", graph.END)

	// Set entry point
	g.SetEntryPoint("HELLO")

	// Start graph processing
	ctx := context.Background()
	for {
		result, state, _ := g.Process(ctx)
		if result.Status.Terminated() {
			break
		}
		fmt.Println("Result:", result)
		fmt.Println("State:", state.Dump())
	}

	// Print final state
	result, state, _ := g.Process(ctx)
	fmt.Println("Result:", result)
	fmt.Println("Final State:", state.Dump())
}
```

above code will output:

```
Result: {PENDING START}
State: map[message:Hello]
Result: {PROCESSING HELLO}
State: map[message:Hello]
Result: {COMPLETED END}
Final State: map[message:Hello]
```

## Example Code

The examples directory contains various examples that demonstrate how to use the
GoTrail. These examples illustrate the library's capabilities and can serve as a
reference for real-world application development.

- [basic_graph](examples/basic_graph/main.go): Demonstrates creating and executing
  a basic graph.
- [basic_graph_struct](examples/basic_graph_struct/main.go): Demonstrates creating
  and executing a basic graph using a struct.
- [openai_chat](examples/openai_chat/main.go): An example of a simple chatbot
  implementation using OpenAI's GPT model.

To run the example, use the following commands:

```bash
# basic_graph example
go run ./examples/basic_graph

# basic_graph_struct example
go run ./examples/basic_graph_struct

# openai_chat example
go run ./examples/openai_chat
```

note that you need to set the `OPENAI_API_KEY` environment variable to run the
`openai_chat` example.

You can find more examples in the [examples](examples) directory.

## License

GoTrail is licensed under the MIT License. See the [LICENSE](LICENSE) file in
the project root for more information.

## Contributing

Contributions are welcome! Please see our [Contributing Guide](CONTRIBUTING.md)
for more details.

