package main

// in this example, we will create a simple chatbot using openai
// the chatbot will use gpt-4-turbo-preview model to generate a response for the given input
// the chatbot will stop if the user input is "quit"
//
// in this example, we can see how to provide a parameter to the graph
//
// the graph will have 3 nodes:
// 1. Input: a node that takes a message from user, and pass it through the state
// 2. LLM: a node that generate a contents for the given input using gpt-4-turbo-preview model
// 3. Quit: a node that checks if the input is quit
//
// the graph will look like this:
// ┌─────┐
// │INPUT│
// └△─┬──┘
//  │┌▽───┐
//  ││QUIT│
//  │└┬───┘
// ┌┴─▽┐
// │LLM│
// └───┘

import (
	"bufio"
	"context"
	"fmt"
	"os"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"

	"github.com/sashabaranov/go-openai"
)

// Input is a node that takes a message from user, and pass it through the state
type Input struct {
	*graph.BaseNode
}

// Pameterize is tell to the graph that this node require a parameter
func (n *Input) Parameterize(_ context.Context, stateSnapshot *graph.State) ([]string, error) {
	// tell to the graph that this node require 1 parameter called "userInput"
	// note that, this function should be idempotent against the stateSnapshot
	// because it can be called multiple times
	return []string{"userInput"}, nil
}

// LLM is a node that generate a contents for the given input
type LLM struct {
	*graph.BaseNode

	Client       *openai.Client
	SystemPrompt openai.ChatCompletionMessage
	Model        string
}

func (n *LLM) Invoke(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
	// chat history from state
	chatHistory := stateSnapshot.Get("chatHistory").([]openai.ChatCompletionMessage)

	// get user input from state
	// it comes from the input node
	input := stateSnapshot.Get("userInput").(string)

	// if chat history is empty, add system prompt
	if len(chatHistory) == 0 {
		chatHistory = append(chatHistory, n.SystemPrompt)
	}

	// add user input to chat history
	chatHistory = append(chatHistory, openai.ChatCompletionMessage{
		Role:    openai.ChatMessageRoleUser,
		Content: input,
	})

	// call the model with chat history
	completion, err := n.Client.CreateChatCompletion(ctx, openai.ChatCompletionRequest{
		Model:    n.Model,
		Messages: chatHistory,
	})
	if err != nil {
		return nil, err
	}

	// add model response to chat history, and pass it through the state
	message := completion.Choices[0].Message
	chatHistory = append(chatHistory, openai.ChatCompletionMessage{
		Role:    message.Role,
		Content: message.Content,
	})
	updateState := graph.NewState(map[string]any{
		"chatHistory": chatHistory,
	})

	return updateState, nil
}

// Quit is a node that checks if the input is quit
// it has own transition implementation which is determine the next node dynamically
type Quit struct {
	*graph.BaseNode

	next string
}

func (n *Quit) Transition(_ context.Context, stateSnapshot *graph.State) (string, error) {
	// get user input from state
	// it comes from the input node
	input := stateSnapshot.Get("userInput").(string)

	// if the input is quit, return END
	if input == "quit" {
		return graph.END, nil
	}

	// if the input is not quit, return next node(should be LLM)
	return n.next, nil
}

func main() {
	// Create a graph
	g, err := createGraph()
	if err != nil {
		panic(err)
	}

	// Process the graph
	fmt.Println("Type 'quit' to exit")
	ctx := context.Background()
	for {
		// Process the graph
		result, state, err := g.Process(ctx)

		// if err is not nil, graph processing has failed
		if err != nil {
			fmt.Println("graph processing failed")
			fmt.Println(err)
			break
		}

		// exit the loop if the graph is terminated
		if result.Status.Terminated() {
			break
		}

		// handle
		if err := handleResult(ctx, g, result, state); err != nil {
			fmt.Println(err)
			break
		}
	}
}

// createGraph creates a graph
func createGraph() (*graph.Graph, error) {
	// create nodes
	nodes := map[string]graph.Node{}
	nodes["LLM"] = &LLM{
		BaseNode: &graph.BaseNode{},

		Model:  openai.GPT4TurboPreview,
		Client: openai.NewClient(os.Getenv("OPENAI_API_KEY")),
		SystemPrompt: openai.ChatCompletionMessage{
			Role:    openai.ChatMessageRoleSystem,
			Content: "Your are a helpful assistant.",
		},
	}
	nodes["INPUT"] = &Input{BaseNode: &graph.BaseNode{}}
	nodes["QUIT"] = &Quit{BaseNode: &graph.BaseNode{}, next: "LLM"}

	// create an empty chat history
	chatHistory := make([]openai.ChatCompletionMessage, 0)

	// create a graph
	g, err := graph.New(
		"gpt4-chatbot",
		// with empty chat history
		graph.WithInitialState(graph.NewState(map[string]any{"chatHistory": chatHistory})),
		// with nodes defined above
		graph.WithNodes(nodes),
	)
	if err != nil {
		return nil, err
	}

	// Set the quit to the next node of the input
	g.AddTransition("INPUT", "QUIT")

	// Set the input to the next node of the LLM
	g.AddTransition("LLM", "INPUT")

	// QUIT node not need AddTransition, because it has own transition implementation
	// it will determine the next node dynamically(one of LLM or END) based on the user input

	// Set the entry point of the graph
	g.SetEntryPoint("INPUT")

	return g, nil
}

// handleResult handles the graph result
func handleResult(_ context.Context, g *graph.Graph, result graph.Result, state *graph.State) error {
	switch result {

	// if the graph is waiting for input, grab input from user
	case graph.Result{Status: graph.GraphStatusWaitInput, CurrentNode: "INPUT"}:
		i, err := grabInput()
		if err != nil {
			return err
		}

		// puts parameters that required by the input node
		g.PutParameter(graph.NewState(map[string]any{
			"userInput": i,
		}))

	// if the graph is processing, and the current node is LLM
	case graph.Result{Status: graph.GraphStatusProcessing, CurrentNode: "LLM"}:
		// print the last message from chat history
		// it shoud be message from the model
		chatHistory := state.Get("chatHistory").([]openai.ChatCompletionMessage)
		if len(chatHistory) == 0 {
			return nil
		}

		// if the last message comes from AI, print it
		lastMessage := chatHistory[len(chatHistory)-1]
		if lastMessage.Role != openai.ChatMessageRoleAssistant {
			fmt.Println("ERROR", "AI does not respond.")
			return nil
		}

		fmt.Println("LLM>", lastMessage.Content)
		fmt.Println("")
	}

	return nil
}

func grabInput() (string, error) {
	var input string
	fmt.Print("You> ")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	if err := scanner.Err(); err != nil {
		return "", err
	}
	input = scanner.Text()

	return input, nil
}
