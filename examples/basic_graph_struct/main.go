package main

// this is a same exaple basic_graph, but we are using the custom node implementation
// to create the graph
//
// in this example, we can see how to define a custom node implementation
//

import (
	"context"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

// Hello is a node that will update the state with "hello" string
type Hello struct {
	// most simple way to declare a node is to embed the BaseNode
	// this will give us the default implementation of the Node interface (Invoke, Transition, Parameterize)
	// and we can override the methods that we want to change
	*graph.BaseNode

	// we can add more fields to the node
	msg string
}

// Invoke is the function that will be called when the node is processed
func (h *Hello) Invoke(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
	// we are not using these in this example
	_ = ctx
	_ = stateSnapshot

	// update the state with the output of the hello node
	// this will be merged into the state of the graph
	updateState := graph.NewState(map[string]any{"msg": "hello"})

	return updateState, nil
}

// Transition is the function that used to determine the next node
func (h *Hello) Transition(ctx context.Context, stateSnapshot *graph.State) (string, error) {
	// we are not using these in this example
	_ = ctx
	_ = stateSnapshot

	// return the next node
	return "WORLD", nil
}

// Parameterize is the function that tell to the graph that the node need parameters
func (h *Hello) Parameterize(ctx context.Context, stateSnapshot *graph.State) ([]string, error) {
	// Hello node does not need any parameters
	return []string{}, nil
}

// World is a node that will get the value of the msg key from the state and update the state with "world" string
type World struct {
	*graph.BaseNode

	msg string
}

func (w *World) Invoke(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
	// we are not using these in this example
	_ = ctx

	// get the value of the msg key from the state
	// it was set by the hello node
	msg := stateSnapshot.Get("msg").(string)

	// update the state with the output of the world node
	msg = fmt.Sprintf("%s %s", msg, "world")
	updateState := graph.NewState(map[string]any{"msg": msg})

	return updateState, nil
}

func (w *World) Transition(ctx context.Context, stateSnapshot *graph.State) (string, error) {
	// we are not using these in this example
	_ = ctx
	_ = stateSnapshot

	// return the next node
	return graph.END, nil
}

func (w *World) Parameterize(ctx context.Context, stateSnapshot *graph.State) ([]string, error) {
	// World node does not need any parameters
	return []string{}, nil
}

func main() {
	// create nodes
	nodes := map[string]graph.Node{}
	nodes["HELLO"] = &Hello{BaseNode: &graph.BaseNode{}, msg: "hello"}
	nodes["WORLD"] = &World{BaseNode: &graph.BaseNode{}, msg: "world"}

	// Create a new graph
	g, err := graph.New(
		"hello-world-graph",
		// we can set the initial state of the graph
		graph.WithInitialState(graph.NewState(map[string]any{"foo": "bar"})),
		// we can also add the nodes to the graph when creating it
		graph.WithNodes(nodes),
	)
	if err != nil {
		panic(err)
	}

	// no need to define invoke and transition, all nodes are have a there own
	// just set the entry point of the graph
	g.SetEntryPoint("HELLO")

	// start the graph until it completes or fails
	ctx := context.Background()
	for {
		// Process the graph
		result, state, err := g.Process(ctx)

		// if err is not nil, graph processing has failed
		if err != nil {
			fmt.Println("graph processing failed")
			fmt.Println(err)
			break
		}

		// exit the loop if the graph is terminated
		if result.Status.Terminated() {
			break
		}

		// otherwise, print the current state and continue processing
		fmt.Printf("current status: %s\ncurrent node: %s\n", result.Status, result.CurrentNode)
		fmt.Printf("state: %+v\n", state.Dump())
		fmt.Println("")
	}

	// print the final state of the graph
	// Process returns the its state when it is terminated
	result, state, _ := g.Process(ctx)
	fmt.Println("graph terminated")
	fmt.Printf("current status: %s\ncurrent node: %s\n", result.Status, result.CurrentNode)
	fmt.Printf("state: %+v\n", state.Dump())
}
