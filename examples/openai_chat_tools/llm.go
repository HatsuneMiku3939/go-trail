package main

import (
	"context"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
	"github.com/HatsuneMiku3939/go-trail/pkg/tools"

	"github.com/sashabaranov/go-openai"
)

// LLM is a node that generate a contents for the given input
type LLM struct {
	*graph.BaseNode

	Client       *openai.Client
	SystemPrompt openai.ChatCompletionMessage
	Model        string
	Tools        []tools.Tool
}

func (n *LLM) Invoke(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
	// chat history from state
	chatHistory := stateSnapshot.Get("chatHistory").([]openai.ChatCompletionMessage)

	// get user input from state
	// it comes from the input node
	input := stateSnapshot.Get("userInput").(string)

	// if chat history is empty, add system prompt
	if len(chatHistory) == 0 {
		chatHistory = append(chatHistory, n.SystemPrompt)
	}

	// add user input to chat history
	chatHistory = append(chatHistory, openai.ChatCompletionMessage{
		Role:    openai.ChatMessageRoleUser,
		Content: input,
	})

	// call the model with chat history
	completion, err := n.Client.CreateChatCompletion(ctx, n.chatCompletionRequest(chatHistory))
	if err != nil {
		return nil, err
	}

	// add model response to chat history, and pass it through the state
	message := completion.Choices[0].Message
	chatHistory = append(chatHistory, openai.ChatCompletionMessage{
		Role:      message.Role,
		Content:   message.Content,
		ToolCalls: message.ToolCalls,
	})

	updateState := graph.NewState(map[string]any{
		"chatHistory": chatHistory,
	})

	return updateState, nil
}

// Transition is a method that determine the next node
// in this example, the next node is INPUT or TOOL
// based on the AI response
func (n *LLM) Transition(_ context.Context, stateSnapshot *graph.State) (string, error) {
	// get the last message from chat history
	chatHistory := stateSnapshot.Get("chatHistory").([]openai.ChatCompletionMessage)

	// get the last message from chat history
	lastMessage := chatHistory[len(chatHistory)-1]

	// if the last message contains tool calls, return TOOL
	if len(lastMessage.ToolCalls) > 0 {
		return "TOOL", nil
	}

	// otherwise, return INPUT
	return "INPUT", nil
}

// chatCompletionRequest is a helper method to create a chat completion request
func (n *LLM) chatCompletionRequest(chatHistory []openai.ChatCompletionMessage) openai.ChatCompletionRequest {
	req := openai.ChatCompletionRequest{
		Model:    n.Model,
		Messages: chatHistory,
		Tools:    make([]openai.Tool, len(n.Tools)),
	}

	for i, t := range n.Tools {
		req.Tools[i] = openai.Tool{
			Type: openai.ToolTypeFunction,
			Function: &openai.FunctionDefinition{
				Name:        t.Name(),
				Description: t.Description(),
				Parameters:  t.Parameters(),
			},
		}
	}

	return req
}
