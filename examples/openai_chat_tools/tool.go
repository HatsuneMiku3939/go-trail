package main

import (
	"context"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
	"github.com/HatsuneMiku3939/go-trail/pkg/tools"

	"github.com/sashabaranov/go-openai"
)

// TOOL is a node that call the tools
type TOOL struct {
	*graph.BaseNode

	Client *openai.Client
	Tools  []tools.Tool
}

func (n *TOOL) Invoke(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
	// chat history from state
	chatHistory := stateSnapshot.Get("chatHistory").([]openai.ChatCompletionMessage)

	// get the last message from chat history
	lastMessage := chatHistory[len(chatHistory)-1]

	// call the tools requested by the AI
	messages := make([]openai.ChatCompletionMessage, len(lastMessage.ToolCalls))
	for i, toolCall := range lastMessage.ToolCalls {
		// find the tool
		tool, err := tools.FindTool(n.Tools, toolCall.Function.Name)
		if err != nil {
			message := openai.ChatCompletionMessage{
				Role:       openai.ChatMessageRoleTool,
				ToolCallID: toolCall.ID,
				Content:    fmt.Sprintf("ERROR: tool not found: %s", toolCall.Function.Name),
			}
			messages[i] = message
			continue
		}

		// call the tool
		content, err := tool.Call(ctx, toolCall.Function.Arguments)
		if err != nil {
			message := openai.ChatCompletionMessage{
				Role:       openai.ChatMessageRoleTool,
				ToolCallID: toolCall.ID,
				Content:    fmt.Sprintf("ERROR: %s", err),
			}
			messages[i] = message
			continue
		}

		// add tool response to chat history
		message := openai.ChatCompletionMessage{
			Role:       openai.ChatMessageRoleTool,
			ToolCallID: toolCall.ID,
			Content:    content,
		}
		messages[i] = message
	}

	// add tool responses to chat history, and pass it through the state
	chatHistory = append(chatHistory, messages...)
	updateState := graph.NewState(map[string]any{
		"chatHistory": chatHistory,
		"numCalls":    len(lastMessage.ToolCalls),
	})

	return updateState, nil
}

// Transition tool node always transition to LLM
func (n *TOOL) Transition(_ context.Context, _ *graph.State) (string, error) {
	return "LLM", nil
}
