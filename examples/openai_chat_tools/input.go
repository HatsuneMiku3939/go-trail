package main

import (
	"context"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

// Input is a node that takes a message from user, and pass it through the state
// it does not need Invoke method, because it just pass the input through the state
type Input struct {
	*graph.BaseNode
}

// Pameterize is tell to the graph that this node require a parameter
func (n *Input) Parameterize(_ context.Context, stateSnapshot *graph.State) ([]string, error) {
	// tell to the graph that this node require 1 parameter called "userInput"
	// this should be idempotent against the stateSnapshot
	return []string{"userInput"}, nil
}

// Transition input node always transition to QUIT
func (n *Input) Transition(_ context.Context, _ *graph.State) (string, error) {
	return "QUIT", nil
}
