package main

import (
	"context"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

// Quit is a node that checks if the input is quit
// it has own transition implementation which is determine the next node dynamically
type Quit struct {
	*graph.BaseNode

	next string
}

func (n *Quit) Transition(_ context.Context, stateSnapshot *graph.State) (string, error) {
	// get user input from state
	// it comes from the input node
	input := stateSnapshot.Get("userInput").(string)

	// if the input is quit, return END
	if input == "quit" {
		return graph.END, nil
	}

	// if the input is not quit, return next node(should be LLM)
	return n.next, nil
}
