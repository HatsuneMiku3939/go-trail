package main

// in this example, we will create a simple chatbot using openai
// the chatbot will use gpt-4-turbo-preview model to generate a response for the given input
// the chatbot will call the tools if the model request it
// the chatbot will stop if the user input is "quit"
//
// not much different from 'openai_chatbot' example,
// but we can see a more advanced usage of the graph
//
// the graph will have 4 nodes:
// 1. Input: a node that takes a message from user, and pass it through the state
// 2. LLM: a node that generate a contents for the given input using gpt-4-turbo-preview model
// 3. TOOL: a node that call the tools
// 4. Quit: a node that checks if the input is quit
//
// the graph will look like this:
// ┌────────┐
// │ INPUT  │
// └△──────┬┘
//  │     ┌▽───┐
//  │     │QUIT│
//  │     └┬───┘
//  │┌────┐│
//  ││TOOL││
//  │└┬───┘│
// ┌┴─▽────▽─┐
// │   LLM   │
// └─────────┘
//
// we have 2 tools in this example, which are DuckDuckGo and Scrap
// - DuckDuckGo: a tool that search the input using DuckDuckGo search engine
// - Scrap: a tool that scrap the text content of the given URL

import (
	"bufio"
	"context"
	"fmt"
	"os"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
	"github.com/HatsuneMiku3939/go-trail/pkg/tools/web"

	"github.com/sashabaranov/go-openai"
)

func main() {
	// Create a graph
	g, err := createGraph()
	if err != nil {
		panic(err)
	}

	// Process the graph
	fmt.Println("Type 'quit' to exit")
	ctx := context.Background()
	for {
		// Process the graph
		result, state, err := g.Process(ctx)

		// if err is not nil, graph processing has failed
		if err != nil {
			fmt.Println("graph processing failed")
			fmt.Println(err)
			break
		}

		// exit the loop if the graph is terminated
		if result.Status.Terminated() {
			break
		}

		// handle
		if err := handleResult(ctx, g, result, state); err != nil {
			fmt.Println(err)
			break
		}
	}
}

// createGraph creates a graph
func createGraph() (*graph.Graph, error) {
	// get the tools
	tools, err := web.All()
	if err != nil {
		return nil, err
	}

	// create nodes
	nodes := map[string]graph.Node{}
	nodes["LLM"] = &LLM{
		BaseNode: &graph.BaseNode{},

		Model:  openai.GPT4TurboPreview,
		Client: openai.NewClient(os.Getenv("OPENAI_API_KEY")),
		SystemPrompt: openai.ChatCompletionMessage{
			Role:    openai.ChatMessageRoleSystem,
			Content: "Your are a helpful assistant.",
		},
		Tools: tools,
	}
	nodes["INPUT"] = &Input{BaseNode: &graph.BaseNode{}}
	nodes["QUIT"] = &Quit{BaseNode: &graph.BaseNode{}, next: "LLM"}
	nodes["TOOL"] = &TOOL{
		BaseNode: &graph.BaseNode{},
		Client:   openai.NewClient(os.Getenv("OPENAI_API_KEY")),
		Tools:    tools,
	}

	// create an empty chat history
	chatHistory := make([]openai.ChatCompletionMessage, 0)

	// create a graph
	g, err := graph.New(
		"gpt4-chatbot",
		// with empty chat history
		graph.WithInitialState(graph.NewState(map[string]any{
			"chatHistory": chatHistory,
			"numCalls":    0,
		})),
		// with nodes defined above
		graph.WithNodes(nodes),
	)
	if err != nil {
		return nil, err
	}

	// all the nodes have Transition method
	// so we don't need to set the transition manually

	// Set the entry point of the graph
	g.SetEntryPoint("INPUT")

	return g, nil
}

// handleResult handles the graph result
func handleResult(_ context.Context, g *graph.Graph, result graph.Result, state *graph.State) error {
	switch result {

	// if the graph is waiting for input, grab input from user
	case graph.Result{Status: graph.GraphStatusWaitInput, CurrentNode: "INPUT"}:
		i, err := grabInput()
		if err != nil {
			return err
		}

		// puts parameters that required by the input node
		g.PutParameter(graph.NewState(map[string]any{
			"userInput": i,
		}))

	// if the graph is processing, and the current node is LLM
	case graph.Result{Status: graph.GraphStatusProcessing, CurrentNode: "LLM"}:
		// print the last message from chat history
		// it shoud be message from the model
		chatHistory := state.Get("chatHistory").([]openai.ChatCompletionMessage)
		if len(chatHistory) == 0 {
			return nil
		}

		// if the last message comes from AI, print it
		lastMessage := chatHistory[len(chatHistory)-1]
		if lastMessage.Role != openai.ChatMessageRoleAssistant {
			fmt.Println("ERROR", "AI does not respond.")
			return nil
		}

		if len(lastMessage.Content) > 0 {
			fmt.Println("LLM>", lastMessage.Content)
		}

		if len(lastMessage.ToolCalls) > 0 {
			fmt.Println("LLM>", "Calling tools...")
			for _, toolCall := range lastMessage.ToolCalls {
				fmt.Printf("    %s(%s)\n", toolCall.Function.Name, toolCall.Function.Arguments)
			}
		}
		fmt.Println("")

	// if the graph is processing, and the current node is TOOL
	case graph.Result{Status: graph.GraphStatusProcessing, CurrentNode: "TOOL"}:
		// get the number of tool calls from state
		numCalls := state.Get("numCalls").(int)

		// get the numCalls messages from chat history
		chatHistory := state.Get("chatHistory").([]openai.ChatCompletionMessage)

		// print the last numCalls messages from chat history
		for i := len(chatHistory) - numCalls; i < len(chatHistory); i++ {
			message := chatHistory[i]
			if message.Role == openai.ChatMessageRoleTool {
				fmt.Println(truncateString(message.Content, 128))
			}
		}
		fmt.Println("")
	}

	return nil
}

func grabInput() (string, error) {
	var input string
	fmt.Print("You> ")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	if err := scanner.Err(); err != nil {
		return "", err
	}
	input = scanner.Text()

	return input, nil
}

func truncateString(str string, length int) string {
	truncated := ""
	count := 0
	for _, char := range str {
		truncated += string(char)
		count++
		if count >= length {
			break
		}
	}
	return truncated + "..."
}
