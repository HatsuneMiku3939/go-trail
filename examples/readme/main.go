package main

import (
	"context"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

func main() {
	// Create graph and set initial state
	g, err := graph.New(
		"hello-graph",
		graph.WithInitialState(graph.NewState(map[string]any{"message": "Hello"})),
	)
	if err != nil {
		panic(err)
	}

	// Add the node
	g.AddNode("HELLO", &graph.BaseNode{})

	// Set transitions
	g.AddTransition("HELLO", graph.END)

	// Set entry point
	g.SetEntryPoint("HELLO")

	// Start graph processing
	ctx := context.Background()
	for {
		result, state, _ := g.Process(ctx)
		if result.Status.Terminated() {
			break
		}
		fmt.Println("Result:", result)
		fmt.Println("State:", state.Dump())
	}

	// Print final state
	result, state, _ := g.Process(ctx)
	fmt.Println("Result:", result)
	fmt.Println("Final State:", state.Dump())
}
