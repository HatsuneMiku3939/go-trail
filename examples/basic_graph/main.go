package main

// in this example, we are going to create a simple graph with 2 nodes
// the first node is a hello node, and the second node is a world node
// the hello node will update the state with "hello" string
// the world node will get the value of the msg key from the state and update the state with "world" string
// the final state of the graph will be {"msg": "hello world"}
//
// in this example, we can see how to create a simple graph with 2 nodes
// and how to update the state of the graph
//
// the graph will look like this:
// START -> HELLO -> WORLD -> END

import (
	"context"
	"fmt"

	"github.com/HatsuneMiku3939/go-trail/pkg/graph"
)

func main() {
	// Create a new graph
	g, err := graph.New(
		"hello-world-graph",
		// we can set the initial state of the graph
		graph.WithInitialState(graph.NewState(map[string]any{"foo": "bar"})),
	)
	if err != nil {
		panic(err)
	}

	// Add the nodes to the graph with their unique names
	// BaseNode is a base implementation of the Node interface
	g.AddNode("HELLO", &graph.BaseNode{})
	g.AddNode("WORLD", &graph.BaseNode{})

	// Set the next node for the hello node
	// simple way to set the next node with no condition
	g.AddTransition("HELLO", "WORLD")
	g.AddTransition("WORLD", graph.END)

	// Set the invoke function for the hello node
	// it will update the state with "hello" string
	g.AddInvoke("HELLO", func(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
		// we are not using these in this example
		_ = ctx
		_ = stateSnapshot

		// update the state with the output of the hello node
		// this will be merged into the state of the graph
		updateState := graph.NewState(map[string]any{"msg": "hello"})

		return updateState, nil
	})

	// set the invoke function for the world node
	// it will get the value of the msg key from the state and update the state with "world" string
	g.AddInvoke("WORLD", func(ctx context.Context, stateSnapshot *graph.State) (*graph.State, error) {
		// we are not using these in this example
		_ = ctx

		// get the value of the msg key from the state
		// it was set by the hello node
		msg := stateSnapshot.Get("msg").(string)

		// update the state with the output of the world node
		msg = fmt.Sprintf("%s %s", msg, "world")
		updateState := graph.NewState(map[string]any{"msg": msg})

		return updateState, nil
	})

	// Set the entry point of the graph
	g.SetEntryPoint("HELLO")

	// start the graph until it completes or fails
	ctx := context.Background()
	for {
		// Process the graph
		result, state, err := g.Process(ctx)

		// if err is not nil, graph processing has failed
		if err != nil {
			fmt.Println("graph processing failed")
			fmt.Println(err)
			break
		}

		// exit the loop if the graph is terminated
		if result.Status.Terminated() {
			break
		}

		// otherwise, print the current state and continue processing
		fmt.Printf("current status: %s\ncurrent node: %s\n", result.Status, result.CurrentNode)
		fmt.Printf("state: %+v\n", state.Dump())
		fmt.Println("")
	}

	// print the final state of the graph
	// Process returns the its state when it is terminated
	result, state, _ := g.Process(ctx)
	fmt.Println("graph terminated")
	fmt.Printf("current status: %s\ncurrent node: %s\n", result.Status, result.CurrentNode)
	fmt.Printf("state: %+v\n", state.Dump())
}
